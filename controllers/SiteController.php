<?php

namespace app\controllers;

use DateTime;
use Yii;
use yii\web\UploadedFile;
use yii2mod\rbac\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\SmartIdForm;
use app\models\User;
use app\models\Callbacks;
use app\models\TemplateForm;
use app\components\Helpers;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\helpers\Url;
use yii\db\Expression;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'smart-id', 'signup', 'test-clave-unica', 'test-callback-clave-unica'],
                'rules' => [

                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['smart-id'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['template'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['test-clave-unica', 'test-callback-clave-unica'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['POST'],
                    'callback' => ['POST'],
                    'user-info' => ['POST'],
                    'test-clave-unica' => ['GET'],
                    'test-callback-clave-unica' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'login' => [
                'class' => 'yii2mod\user\actions\LoginAction'
            ],
            'logout' => [
                'class' => 'yii2mod\user\actions\LogoutAction'
            ],
            'signup' => [
                'class' => 'yii2mod\user\actions\SignupAction',
                'on afterSignup' => function ($event) {
                    $master_user = User::findOne(Yii::$app->user->id);
                    $signup_user = $event->getForm()->getUser();
                    $slave_user = User::findOne($signup_user->id);
                    $slave_user->api_key_toc = $master_user->api_key_toc;
                    $slave_user->template_id = $master_user->template_id;
                    $slave_user->save(false);
                },
            ],
            'request-password-reset' => [
                'class' => 'yii2mod\user\actions\RequestPasswordResetAction'
            ],
            'password-reset' => [
                'class' => 'yii2mod\user\actions\PasswordResetAction'
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTemplate()
    {
        $user = User::findOne(\Yii::$app->user->id);
        if (!$user || $user->api_key_toc == "") {
            throw new HttpException(207, 'Usuario no posee Api key para utilizar el servicio.');
        }
        $token = $user->api_key_toc;
        $model = new TemplateForm();
        $model->color_background = "#ffffff";
        $model->color_font = "#00000";
        $model->color_font_primary = "#ffffff";
        $model->color_font_secondary = "#ffffff";
        $model->color_primary = "#2f2fd0";
        $model->color_secondary = "#2626a1";
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $image = UploadedFile::getInstance($model, 'image');
                if (!is_null($image)) {
                    $img_path = $image->tempName;
                    $img_data = file_get_contents($img_path);
                    $model->logo = base64_encode($img_data);
                }
                $headers = [];
                $headers2 = [];
                $headers['Authorization'] = 'Bearer ' . $token;
                $headers['Content-Type'] = 'application/json';
                foreach ($headers as $k => $v) {
                    $headers2[] = $k . ': ' . $v;
                }
                $payload = Json::encode($model->attributes);
                try {
                    Yii::info($headers2);
                    Yii::info($payload);
                    $response = Helpers::curl_get_contents(Yii::$app->params['endpoint_template'], $headers2, 'POST', $payload);
                    Yii::info($response);
                    if (isset($response->data)) {
                        $user = User::findOne(Yii::$app->user->id);
                        $user->template_id = $response->data->id;
                        $user->save(false);
                    }
                } catch (yii\base\Exception $e) {
                    $response = $e->getTraceAsString();
                }
                Yii::info($payload);
                Yii::$app->session->setFlash('templateFormSubmitted');
            }
        }

        return $this->render('template', [
            'model' => $model,
        ]);
    }


    /**
     * Displays SmartId.
     *
     * @return string
     */
    public function actionSmartId()
    {
        $user = User::findOne(\Yii::$app->user->id);
        if (!$user || $user->api_key_toc == "") {
            throw new HttpException(207, 'Usuario no posee Api key para utilizar el servicio.');
        }
        $token = $user->api_key_toc;
        $template = $user->template_id;
        $model = new SmartIdForm();
        $model->name = true;
        $model->last_name = true;
        $model->full_name = true;
        $model->rut = true;
        $model->serial_number = true;
        $model->ci_expiration = true;
        $model->age = true;
        $model->gender = true;
        $model->callback_url = Url::to(['site/callback'], true);
        $qr_image = null;
        $qr_url = null;
        $response = null;
        if ($model->load(Yii::$app->request->post())) {
            $headers = [];
            $headers2 = [];
            $headers['Authorization'] = 'Bearer ' . $token;
            $headers['Content-Type'] = 'application/json';
            foreach ($headers as $k => $v) {
                $headers2[] = $k . ': ' . $v;
            }
            $filters = [];
            foreach ($model->attributes as $attribute => $value) {
                if ($value == 1 || $value == '1') {
                    $filters[$attribute] = true;
                }
            }
            $payload = Json::encode([
                'callbackUrl' => $model->callback_url,
                'filters' => $filters,
                'cID' => $user->id,
                'frontCustomizationId' => $template,
                'verificationType' => 'liveness',
                'urlRedirect' => Url::to(['site/exito'], true),
            ]);
            try {
                Yii::info($headers2);
                Yii::info($payload);
                $response = Helpers::curl_get_contents(Yii::$app->params['endpoint'], $headers2, 'POST', $payload);
                Yii::info($response);
                if (isset($response->data)) {
                    if (isset($response->data->qr_code)) {
                        $qr_image = $response->data->qr_code;
                        $qr_url = $response->data->qr_content;
                    }
                }
            } catch (yii\base\Exception $e) {
                $response = $e->getTraceAsString();
            }

            Yii::$app->session->setFlash('smartIdFormSubmitted');
        }
        return $this->render('smart_id', ['model' => $model, 'qr_image' => $qr_image, 'qr_url' => $qr_url, 'response' => $response]);
    }


    public function actionCallback()
    {
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            Yii::info($data);
            $callback = new Callbacks();
            $callback->toc = Json::encode($data['toc']);
            $callback->cID = $data['cID'];
            $callback->token = $data['token'];
            $callback->save(false);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'status' => 200,
            ];
        }
    }


    public function actionUserInfo()
    {
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            Yii::info($data);
            $expression = new Expression("DATE_SUB(NOW(), INTERVAL 5 SECOND)");
            $callback = Callbacks::find()->where(['cID' => $data['cID']])->andWhere(['>=', 'created_at', $expression])->one();
            if ($callback) {
                $html = "";
                $data_callback = Json::decode($callback->toc);
                foreach ($data_callback as $key => $value) {
                    $html .= $key . ": " . $value . "\n";
                }
                $html .= 'TOC TOKEN: ' . $callback->token;
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'status' => 200,
                    'html' => $html
                ];
            }

        }
    }

    /**
     * Displays exito page.
     *
     * @return string
     */
    public function actionExito()
    {
        return $this->render('exito');
    }

    public function actionTestClaveUnica()
    {
        // este es la URL del endpoint a utilizar para la recepcion del callback y del redirect (opcional)
        // esta debe cambiarse acorde al desarrollo
        $endpoint_url = 'https://webhook.site/f02dd18e-c36b-47bd-9ad3-51295b950e9c';

        $fecha = new DateTime();
        $salt = "Este es un string que solo sera conocido por Bancovice";
        $timestamp = $fecha->getTimestamp(); // Este timestamp se utilizara para validar la duracion de la url
        $unique_id = md5($salt . $timestamp); // Este id se debe almacenar ya que sera consultado junto con la fecha
        echo "timestamp:" . $timestamp;
        echo "<br>";
        echo "unique_id:" . $unique_id;
        echo "<br>";

        /**
         * IDealmente se deberia crear una pequeña tabla temporal con la data del timestamp y del id unico, asi al recibir
         * el POST de datos del callback, primero validaria la duracion del timestamp, y si es valida aun,
         * consultaria los registros donde el timestamp y el id unico coincidan, si el registro existe, entonces es valido
         */

        /* Aca genero las url con los datos de validacion */
        $callback_url = $endpoint_url .
            '?timestamp=' . $timestamp . // Aca defino la primera variable de la url
            '&uuid=' . $unique_id; // Aca defino la segunda variable

        /* SOLO DE EJMPLO, PUEDE OBVIARSE */
        $redirect_url = $endpoint_url .
            '?action=redirect'; // Aca defino este parametro para diferenciar la accion

        echo "callback_url:" . $callback_url;
        echo "<br>";
        echo "redirect_url:" . $redirect_url;
        echo "<br>";
        $curl = curl_init();

        $postfields = [
            'apiKey' => '5c8061ce04ab4b52afd89bb037ca15c0',
            'callbackUrl' => $callback_url,
            'tokenUAF' => '12345678',
            'environment' => 'sandbox',
            'redirectUrl' => $redirect_url
        ];

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://claveunica.7oc.cl/request_token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postfields,
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $decoded_response = json_decode($response, true);

        if (isset($decoded_response['status']) && $decoded_response['status'] == 200) {
            /*
             * En este paso es ideal el almacenamiento de las variables de validacion, ya que se consumio
             * correctamente el servicio de clave unica y la url de callback pasa a ser valida en el sistema
             * EJ:
             * "INSERT INTO tbl_validacion_callbacks(stored_timestamp,stored_uuid)
             * VALUES ({$timestamp}, {$unique_id});"
             */
            echo "terminado con exito <br> url: https://claveunica.7oc.cl/auth?access_token=" . $decoded_response['access_token'];
        } else {
            echo "ocurrio un problema </br> la respuesta es:" . $response;
        }
        return "";
    }

    public function actionTestCallbackClaveUnica()
    {
        if (!isset($_GET['timestamp']) || !isset($_GET['uuid'])) {
            return "faltan parametros requeridos";
        }
        $timestamp = $_GET['timestamp'];
        $unique_id = $_GET['uuid'];
        /**
         * Aca recibidos los parametros, como medida de seguridad se deben sanitizar y validar,
         * previo a realizar las comparaciones y consultas en registros, si no coincide formato
         * tambien deberia descartarse la solicitud
         */

        if ($timestamp <= strtotime("-50 minutes")) {
            // esta es la primera validacion, la duracion de la url, solo son 30 minutos,
            // si pasa este tiempo ya no sera aceptada, aunque los datos sean validos
            return "Url caducada, ya no es posible acceder.";
        }

        /**
         * aca como medida extra puedo validar la integrad de ambos parametros recibidos y validar si coincide
         * con la logica interna de generacion de ids
         */
        $salt = "Este es un string que solo sera conocido por Bancovice"; // Mismo string utilizado para generar el id
        $generated_unique_id = md5($salt . $timestamp);

        if ($generated_unique_id !== $unique_id) {
            return "Solicitud no reconocida!.";
        }

        /**
         * en esta parte ya pase varias validaciones, ahora deberia consultar si la data de la url
         * (el parametro timestamp y el parametro uuid) hace match con la data almacenada anteriormente
         * en alguna tabla temporal para consultar, si los parametros coinciden entonces recibo la data del POST
         * EJ:
         * "SELECT * FROM tbl_validacion_callbacks
         * WHERE stored_timestamp = {$timestamp} AND stored_uuid = {$unique_id};"
         *
         * Si la bbdd contiene un registro, entonces es completamente valida la URL, y por tanto la data
         */

        $data_coincide = true;

        if ($data_coincide) {
            /*
             * En este paso de igual forma se puede validar el formato de la respuesta, validando
             * headers y el mapeo de las llaves del objeto JSON recibido, el cual tiene la siguiente
             * estructura:
             * '''
                {
                  "run": 123456789,
                  "dv": "k",
                  "name": [
                    "Jhon",
                    "Michale"
                  ],
                  "last_name": [
                    "Doe",
                    "Some"
                  ],
                  "email": "",
                  "tokenUAF": "12345678",
                  "toc_token": "1234567890abcdefghij"
                }
             * '''
             * cualquier otra estructura deberia ser rechazada, y incluso mas, se podria validar los valores
             * de las llaves con algun parseador o expresiones regulares
             */
            // aca almacenamos la informacion del post, luego de que sabemos que esta validada
            $post = file_get_contents('php://input'); //se utiliza esto porque viene como JSON
            return $post;
        }
        return "No existen registros asociados";
    }
}
