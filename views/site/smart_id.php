<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\SmartIdForm */
/* @var $qr_image string */
/* @var $qr_url string */
/* @var $response string|Array */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$url = Url::to(['site/user-info'], true);
$user_id = Yii::$app->user->identity->id;
$script = <<<JS
var interval = null;
$(document).ready(function() {
    interval = setInterval(function() {
        getUserInfo();
    }, 5000);

});
function getUserInfo() {
    $.ajax({
        type: "POST",
        url: "{$url}",
        data: {
            "cID": "{$user_id}"
        },
        success: function(response) {
            if (response.status == 200) {
                swal("Información obtenida correctamente", response.html, "success");
            }
        },
        err: function(err) {}
    });
};
JS;

$this->registerJs($script);

$this->title = 'Generador de QR';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">

                        <h2>SmartId 7OC</h2>
                        <p>Seleccione las opciones que desea extraer y presione generar para crear un codigo QR que te guiara en el proceso.</p>
                        <br/>

                        <?php $form = ActiveForm::begin(['id' => 'smart-id-form']); ?>
                        <?= $form->field($model, 'name')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'last_name')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'full_name')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'rut')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'serial_number')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'ci_expiration')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'age')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'gender')->checkbox(['readonly' => (!Yii::$app->user->isGuest && Yii::$app->user->id != '100') ? true : false]) ?>
                        <?= $form->field($model, 'callback_url')->textInput(['disabled' => (!Yii::$app->user->can('superadmin')) ? true : false]) ?>

                        <div class="form-group text-center">
                            <?= Html::submitButton('Generar Codigo QR', ['class' => 'btn btn-success btn-lg ', 'name' => 'smart-id-button']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="col-lg-6 text-center" >
                        <?php if (Yii::$app->session->hasFlash('smartIdFormSubmitted')) : ?>
                            <?php if($qr_image != null): ?>
                                <div class="alert alert-success">
                                    Gracias por utilizar el generador, el codigo esta mas abajo.
                                </div>
                                <p>Haz click en el boton <i class="glyphicon glyphicon-copy"></i> para copiar el link</span></p>
                                <div class="input-group">
                                    <?= Html::input('text','qr-url-input',$qr_url,['id'=>'input','readonly'=>true,'class'=>'form-control']) ?>
                                    <span class="input-group-btn">
                                        <?= Html::button('<i class="glyphicon glyphicon-copy"></i>',['onclick'=>'myFunction()','class'=>'btn btn-primary']) ?>
                                    </span>
                                </div>
                                <p><?= Html::img('data:image/png;base64,' . $qr_image);?></p>
                            <?php
                                $url="data:image/png;base64," . $qr_image;
                                $date = date("Y-m-d_H:i:s");
                                echo Html::a('Descargar Qr', $url, ['download'=>'qr_'.$date.'.jpg', 'class'=> 'btn btn-primary btn-lg btn-block' ]);
                            else: ?>
                                <div class="alert alert-danger">
                                    Ha ocurrido un error al obtener el codigo QR, Recuerda que debes seleccionar al menos una de las opciones del formulario, intenta nuevamente y si aun persiste el problema comunicate con TI.
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
