<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Exito!';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Se ha realizado con exito la validacion de datos, puedes volver al sitio
    </p>
</div>
