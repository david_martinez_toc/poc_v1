<?php

use kartik\color\ColorInput;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TemplateForm */
/* @var $form ActiveForm */

$this->title = 'Personalizacion de Interfaz';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-template">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <?php if (Yii::$app->session->hasFlash('templateFormSubmitted')) : ?>
                        <div class="alert alert-success">
                            Se han guardado tus configuraciones de apariencia.
                        </div>
                    <?php endif; ?>
                    <div class="col-lg-6">

                        <h2>Apariencia</h2>
                        <p>Seleccione los colores e imagen que se asociara a sus requerimientos de Smart Id.</p>
                        <br/>

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
                        ]);   ?>
                        <?= $form->field($model, 'color_background')->widget(ColorInput::classname(), [
                            'options' => ['placeholder' => 'Seleccione color ...'],
                        ]) ?>
                        <?= $form->field($model, 'color_font')->widget(ColorInput::classname(), [
                            'options' => ['placeholder' => 'Seleccione color ...'],
                        ]) ?>
                        <?= $form->field($model, 'color_primary')->widget(ColorInput::classname(), [
                            'options' => ['placeholder' => 'Seleccione color ...'],
                        ]) ?>
                        <?= $form->field($model, 'color_font_primary')->widget(ColorInput::classname(), [
                            'options' => ['placeholder' => 'Seleccione color ...'],
                        ]) ?>
                        <?= $form->field($model, 'color_secondary')->widget(ColorInput::classname(), [
                            'options' => ['placeholder' => 'Seleccione color ...'],
                        ]) ?>
                        <?= $form->field($model, 'color_font_secondary')->widget(ColorInput::classname(), [
                            'options' => ['placeholder' => 'Seleccione color ...'],
                        ]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Aplicar', ['class' => 'btn btn-primary']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div><!-- site-template -->
