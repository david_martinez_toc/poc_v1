<?php

/* @var $this yii\web\View */

$this->title = 'Smart Id 7OC';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>¡Bienvenido a Identidad Digital!</h1>

        <p class="lead">Realizaras el enrolamiento desde la generacion de un codigo Qr en unos simples pasos.</p>

<!--         <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
 -->    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Paso 1</h2>

                <p>Genera tu codigo QR o copia el enlace generado</p>
                <div class="text-center">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3ier2nnSh7yFF8xUSXjueG9oVq4vV58z46Q&usqp=CAU"></img>
                </div>
            </div>
            <div class="col-lg-4">
                <h2>Paso 2</h2>

                <p>Escanea el codigo QR que aparece a continuación o dirigete a la URL previamente copiada</p>
                <div >
                <img width="320px" src="https://image.freepik.com/vector-gratis/smartphone-su-concepto-mano-escanear-codigo-qr_147754-197.jpg"></img>
                </div>
            </div>
            <div class="col-lg-4">
                <h2>Paso 3</h2>

                <p>¡Realiza el enrolamiento!</p>
                <div class="text-center">
                <img width="350px" src="https://image.freepik.com/vector-gratis/smartphone-mujer-reconocimiento-escaneo-biometrico_24908-56395.jpg"></img>
                </div>
            </div>
        </div>
        <br/>
        <div class="text-center">
        <p><a class="btn btn-lg btn-success" href="/site/smart-id">Ir a Smart ID</a></p>
        </div>
    </div>
</div>
