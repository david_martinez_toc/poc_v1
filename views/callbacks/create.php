<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Callbacks */

$this->title = 'Create Callbacks';
$this->params['breadcrumbs'][] = ['label' => 'Callbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callbacks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
