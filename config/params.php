<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'endpoint' => 'https://id-backend.7oc.cl/client/generate_qr',
    'endpoint_template' => 'https://id-backend.7oc.cl/client/customize_app',
];
