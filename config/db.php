<?php

return [
    'class' => 'yii\db\Connection',
    'charset' => 'utf8',
    // esto es en dev
    'dsn' => 'mysql:host=ec2-3-138-254-78.us-east-2.compute.amazonaws.com;dbname=smart_id_poc',
    'username' => 'myuser',
    'password' => 'myuser1234',
    // esto es en prod (descomentar para prod)
//    'dsn' => 'mysql:host=localhost;dbname=smart_id_poc',
//    'username' => 'root',
//    'password' => 'Admin1234',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
