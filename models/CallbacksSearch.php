<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Callbacks;

/**
 * CallbacksSearch represents the model behind the search form of `app\models\Callbacks`.
 */
class CallbacksSearch extends Callbacks
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['toc', 'cID', 'token', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        if(\Yii::$app->user->can('superadmin')){
            $query = Callbacks::find();
        }else{
            $user = User::findOne(\Yii::$app->user->id);
            $ids = [];
            $users = User::find()->select('id')->where(['api_key_toc' => $user->api_key_toc])->all();
            \Yii::info($users);
            foreach ($users as $_user) {
                $ids[] = $_user->id;
            }
            $query = Callbacks::find()->where(['cID' => $ids]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'toc', $this->toc])
            ->andFilterWhere(['like', 'cID', $this->cID])
            ->andFilterWhere(['like', 'token', $this->token]);

        return $dataProvider;
    }
}
