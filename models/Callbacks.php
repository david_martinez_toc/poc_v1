<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "callbacks".
 *
 * @property int $id
 * @property string|null $toc
 * @property string|null $cID
 * @property string|null $token
 * @property string|null $created_at
 */
class Callbacks extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'callbacks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['toc'], 'string'],
            [['created_at'], 'safe'],
            [['cID', 'token'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'toc' => 'Toc',
            'cID' => 'C ID',
            'token' => 'Token',
            'created_at' => 'Created At',
        ];
    }

    public function behaviors()
    {
        return [

            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],           
        ];
    }
}
