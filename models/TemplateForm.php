<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ThemeForm is the model behind the theme form.
 */
class TemplateForm extends Model
{
    public $image;
    public $logo;
    public $color_background;
    public $color_font;
    public $color_primary;
    public $color_font_primary;
    public $color_secondary;
    public $color_font_secondary;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['logo'],'string'],
            [['logo','color_background','color_font','color_primary','color_font_primary','color_secondary','color_font_secondary'], 'safe'],
            [['color_background','color_font','color_primary','color_font_primary','color_secondary','color_font_secondary'], 'string'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['image'], 'file', 'maxSize'=>'100000'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'logo' => 'Imagen de Logo',
            'image' => 'Imagen de Logo',
            'color_background' => 'Color de Fondo',
            'color_font' => 'Color de Tipografia',
            'color_primary' => 'Color Primario',
            'color_font_primary' => 'Color Tipografia Primaria',
            'color_secondary' => 'Color Secundario',
            'color_font_secondary' => 'Color Tipografia Secundaria',
        ];
    }

}
