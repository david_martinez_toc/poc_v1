<?php

use yii\db\Migration;

/**
 * Class m210511_034849_add_column_template_id_table_user
 */
class m210511_034849_add_column_template_id_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'template_id', $this->string(128));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'template_id');
    }
}
