<?php

use yii\db\Migration;

/**
 * Class m210510_171523_add_column_apikey_table_user
 */
class m210510_171523_add_column_apikey_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'api_key_toc', $this->string(512));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'api_key_toc');
    }

}
