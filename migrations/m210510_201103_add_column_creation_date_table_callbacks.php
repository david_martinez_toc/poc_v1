<?php

use yii\db\Migration;

/**
 * Class m210510_201103_add_column_creation_date_table_callbacks
 */
class m210510_201103_add_column_creation_date_table_callbacks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%callbacks}}', 'created_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%callbacks}}', 'created_at');
    }
}
