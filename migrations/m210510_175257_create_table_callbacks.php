<?php

use yii\db\Migration;

/**
 * Class m210510_175257_create_table_callbacks
 */
class m210510_175257_create_table_callbacks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%callbacks}}', [
            'id' => $this->primaryKey(),
            'toc' => $this->text(),
            'cID' => $this->string(128),
            'token' => $this->string(128)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%callbacks}}');
    }
}
