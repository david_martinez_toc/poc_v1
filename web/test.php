<!DOCTYPE html>
<html>
<body>

<?php

$fecha = new DateTime();
$salt = "Este es un string que solo sera conocido por Bancovice";
$timestamp = $fecha->getTimestamp(); // Este timestamp se utilizara para validar la duracion de la url
$unique_id = md5($salt.$timestamp); // Este id se debe almacenar ya que sera consultado junto con la fecha
echo "timestamp:".$timestamp;
echo "<br>";
echo "unique_id:".$unique_id;
echo "<br>";

/**
 * IDealmente se deberia crear una pequeña tabla con la data del timestamp y del id unico, asi al recibir
 * el POST de datos del callback, primero validaria la duracion del timestamp, y si es valida aun,
 * consultaria los registros donde el timestamp y el id unico coincidan.
 */

/* Aca genero las url con los datos de validacion */
$callback_url ='https://webhook.site/5120fc3a-bfd5-4a9d-857a-aa78f473eba5'.
    '?timestamp='.$timestamp. // Aca defino la primera variable de la url
    '&uuid='.$unique_id; // Aca defino la segunda variable

$redirect_url = 'https://webhook.site/5120fc3a-bfd5-4a9d-857a-aa78f473eba5'.
    '?action=redirect'; // Aca defino este parametro para diferenciar los diferentes action

echo "callback_url:".$callback_url;
echo "<br>";
echo "redirect_url:".$redirect_url;
echo "<br>";
$curl = curl_init();

$postfields =[
    'apiKey' => '5c8061ce04ab4b52afd89bb037ca15c0',
    'callbackUrl' => $callback_url,
    'tokenUAF' => '12345678',
    'environment' => 'sandbox',
    'redirectUrl' => $redirect_url
];

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://claveunica.7oc.cl/request_token',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => array(),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
echo '</br>Termino';
?>

</body>
</html>
