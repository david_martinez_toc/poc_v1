function myFunction() {
    let inputEl = document.getElementById("input");
    inputEl.select();                                    
    inputEl.setSelectionRange(0, inputEl.value.length); 

    const successful = document.execCommand('copy');   

    if(successful) {
        swal("Excelente", "¡Link copiado con exito!", "success");
    } else {
        swal("Algo ha salido mal", "Genera otra vez el codigo QR", "error");
    }
}